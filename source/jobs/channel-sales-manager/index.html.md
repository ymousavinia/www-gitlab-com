---
layout: job_page
title: "Channel Sales Manager"
---

The Channel Sales Manager (Latin America or APAC timezones) is responsible for sales enablement of their assigned channel, and managing their channel to achieve stated sales targets.  

## Responsibilities

- Act as a liaison between the GitLab and assigned channel partners.
- Build, maintain, and manage relationships with current and prospective channel partners.  
- Establish productive, professional relationships with key personnel in assigned partner accounts.
- Coordinate the involvement of GitLab personnel,including support, service, and management resources, in order to meet partner performance objectives and partners’ expectations.
- Sell through partner organizations to end users in coordination with partner sales resources.
- Meet assigned targets for profitable sales volume and strategic objectives in assigned partner accounts.
- Manage direct/channel conflict by fostering excellent communication between the channel and direct teams
- Participate in a planning process that develops mutual performance objectives, financial targets, and critical milestones associated with a productive partner relationship.
- Proactively assess, clarify, and validate partner needs on an ongoing basis.


## Requirements for Applicants

- Previous experience of driving channel sales ideally within the same product category and channel.
- Experience selling in the Software Development Tools and/or Application Lifecycle Management space
- Experience selling Open Source Solutions
- 5+ years of experience with B2B sales
- Interest in GitLab, and open source software
- Effective communicator with excellent interpersonal skills and an ability to build strong relationships with partners and GitLab teams.
- Strong personal network within the industry.
- Driven, highly motivated and results driven.
- You share our [values](/handbook/values), and work in accordance with those values.
